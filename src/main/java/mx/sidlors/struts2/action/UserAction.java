package mx.sidlors.struts2.action;


import mx.sidlors.struts2.service.bo.UserBo;
import com.opensymphony.xwork2.ActionSupport;
 
public class UserAction extends ActionSupport{

    //DI via Spring
    UserBo userBo;
    
    public UserBo getUserBo() {
        return userBo;
    }

    public void setUserBo(UserBo userBo) {
        this.userBo = userBo;
    }

    public String execute() throws Exception {
        
        userBo.printUser();
        return SUCCESS;
        
    }
}
