
## Struts 2

Para comenzar a usar Struts 2 crearemos una aplicación web usando Maven para administrar las dependencias de artefactos. Puede consultar todas las aplicaciones de ejemplo del repositorio Struts 2 GitHub en struts-examples.

Cree la aplicación web Struts 2 usando Maven para administrar artefactos y construir la aplicación
Este tutorial asume que usted sabe cómo crear una aplicación web Java que use Maven para administrar artefactos y construir el archivo de archivo (war) de la aplicación web.



[Getting started Oficial](https://struts.apache.org/getting-started/how-to-create-a-struts2-web-application.html)


### Generar y ejecutar la aplicación

Ejecute mvn jetty: ejecute para ejecutar la aplicación web utilizando el complemento jetty maven.

Vea la consola donde debería ver numerosos mensajes de depuración que le indican que el marco Struts 2 se está incluyendo en la aplicación web basic-struts2.

Abra un navegador web y vaya a http: // localhost: 8080 / basic-struts / index.
acción (tenga en cuenta que index.action no index.jsp al final de la URL).

Debería ver la misma página web que cuando vaya a http: // localhost: 8080 / basic-struts / index.jsp. 


Vea los mensajes de registro escritos en la consola y debería encontrar varios que analicen index.action e index.jsp:


### Integracion con Spring 2

A continuacion la integracion de spring2 en este proyecto y esta basado en un pequeño tutorial de [aqui](https://www.mkyong.com/struts2/struts-2-spring-integration-example/)

### Paso 0, Dependencias en el pom.xml


Aqui usaremos las siguientes dependencias en el pom

```xml
	<project>
    <modelVersion>4.0.0</modelVersion>
    <groupId>mx.sidlors</groupId>
    <artifactId>demo-struts2</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>war</packaging>

    <name>demo-struts2</name>
    <url>http://maven.apache.org</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>1.8</java.version>
        <struts2.version>2.1.8</struts2.version>
        <log4j2.version>2.10.0</log4j2.version>
    </properties>

    <build>
		<finalName>basic-struts</finalName>
        <plugins>
            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>9.4.7.v20170914</version>
                <configuration>
                    <webApp>
                        <contextPath>/${build.finalName}</contextPath>
                    </webApp>
                    <stopKey>CTRL+C</stopKey>
                    <stopPort>8999</stopPort>
                    <scanIntervalSeconds>10</scanIntervalSeconds>
                    <scanTargets>
                        <scanTarget>src/main/webapp/WEB-INF/web.xml</scanTarget>
                    </scanTargets>
                </configuration>
            </plugin>
        </plugins>
    </build>

<dependencies>

<!-- Struts 2 -->
	<dependency>
      <groupId>org.apache.struts</groupId>
	  <artifactId>struts2-core</artifactId>
	  <version>${struts2.version}</version>
    </dependency>

	<!-- Spring framework --> 
	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring</artifactId>
		<version>2.5.6</version>
	</dependency>

	<dependency>
		<groupId>org.springframework</groupId>
		<artifactId>spring-web</artifactId>
		<version>2.5.6</version>
	</dependency>

	<!-- Struts 2 + Spring plugins -->
	<dependency>
      <groupId>org.apache.struts</groupId>
	  <artifactId>struts2-spring-plugin</artifactId>
	  <version>2.1.8</version>
    </dependency>

	<!-- junit  framework --> 

    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>3.8.1</version>
        <scope>test</scope>
    </dependency>
</dependencies>
</project>


```


### Paso 1 Spring Listener

Configuraremos el Spring listener “org.springframework.web.context.ContextLoaderListener” en el web.xml 


```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app>
  <display-name>Struts 2 Web Application</display-name>
  
  <filter>
	<filter-name>struts2</filter-name>
	  <filter-class>
	     org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter
	  </filter-class>
  </filter>
  
  <filter-mapping>
	<filter-name>struts2</filter-name>
	<url-pattern>/*</url-pattern>
  </filter-mapping>
 
  <listener>
    <listener-class>
      org.springframework.web.context.ContextLoaderListener
    </listener-class>
  </listener>
  
</web-app>
```
### Register Spring Bean

Se registraran todos los beans Spring’s en el archivo applicationContext.xml 
que se localizen en este xml automaticamente.

applicationContext.xml


```xml
<beans xmlns="http://www.springframework.org/schema/beans"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.springframework.org/schema/beans
http://www.springframework.org/schema/beans/spring-beans-2.5.xsd">
 
	<bean id="userBo" class="com.mkyong.user.bo.impl.UserBoImpl" />
	
	<bean id="userSpringAction" class="com.mkyong.user.action.UserSpringAction">
		<property name="userBo" ref="userBo" />	
	</bean>
 
</beans>
```
### Struts.xml
Aqui se declaran todas las relaciones.


```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE struts PUBLIC
"-//Apache Software Foundation//DTD Struts Configuration 2.0//EN"
"http://struts.apache.org/dtds/struts-2.0.dtd">
 
<struts>
 	<constant name="struts.devMode" value="true" />
 	
	<package name="default" namespace="/" extends="struts-default">
		
		<action name="userAction" 
			class="mx.sidlors.struts2.action.UserAction" >
			<result name="success">pages/user.jsp</result>
		</action>
		
		<action name="userSpringAction" 
			class="userSpringAction" >
			<result name="success">pages/user.jsp</result>
		</action>
		
	</package>
	
</struts>
```
### Ejemplo


Ahora, todo el trabajo de integración de Struts 2 y Spring está hecho, veamos el siguiente caso de uso para acceder al bean "userBo" de Spring.

Caso 1: Haga que Spring actúe como la clase de acción Struts 2 y acceda al bean Spring.
Caso 2: Acceda al bean Spring's en la clase de acción Struts 2.

#### Caso 1

En este ejemplo, userSpringAction actúa como la clase de acción Struts 2, y 
puede DI el bean UserBo de Spring con la forma normal de Spring.

```xml

//struts.xml
<action name = "userSpringAction"
class = "userSpringAction">
<result name = "success"> pages / user.jsp </result>
</action>
```

Aqui tenemos el ApplicationContext.xml

```xml
//applicationContext.xml
<bean id = "userSpringAction" class = "com.mkyong.user.action.
UserSpringAction ">
<property name = "userBo" ref = "userBo" />
</bean>
```


Para acceder a esta acción, use la URL: http://localhost:8080/basic-struts/userSpringAction

#### Caso 2

De forma predeterminada, Spring listener habilita el "wired automático" 
haciendo coincidir el nombre del bean. Por lo tanto, pasará el bean "userBo" de Spring a UserAction a través de setUserBo () automáticamente.

Veamos a continuación la acción Struts 2:

http://localhost:8080/basic-struts/userAction

